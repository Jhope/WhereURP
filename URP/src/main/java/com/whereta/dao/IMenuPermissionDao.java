package com.whereta.dao;

import com.whereta.model.MenuPermission;

import java.util.Set;

/**
 * Created by vincent on 15-8-27.
 */
public interface IMenuPermissionDao {

    Set<Integer> selectPermissionIdSet(int menuId);

    void deleteByPerId(int perId);

    void deleteByMenuId(int menuId);

    int addMenuPermission(MenuPermission menuPermission);

}
