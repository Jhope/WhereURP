package com.whereta.dao;

import com.github.pagehelper.PageInfo;
import com.whereta.model.Account;

import java.util.Map;
import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 16:22
 */
public interface IAccountDao {

    Account get(int id);

    Account getByAccount(String account);

    int save(Account account);

    int delete(int accountId);

    PageInfo<Map> selectAccountIdManage(String querySql, int pageNow, int pageSize);

    PageInfo<Map> selectAccountManage(Set<Integer> depIdSet, Set<Integer> excludeAccountIdSet, int pageNow, int pageSize);
}
