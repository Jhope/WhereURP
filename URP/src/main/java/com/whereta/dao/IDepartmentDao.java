package com.whereta.dao;

import com.whereta.model.Department;

import java.util.List;

/**
 * @author Vincent
 * @time 2015/9/2 10:10
 */
public interface IDepartmentDao {

    List<Department> selectAll();

    Department get(List<Department> list, int id);

    int create(Department department);

    int delete(int depId);

    int update(Department department);

}
